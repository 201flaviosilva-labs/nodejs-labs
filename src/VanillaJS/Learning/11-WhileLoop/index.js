/*
while (condition) {    
   code block
}
*/

console.log("Ciclo While");

let i = 1; //declaração da variavel contadora
while (i<5){ //condição
//Tal como o ciclo for isto irá repetir quandot for true
  console.log("Repetição: " + i);
  i++; //Soma da variavel
}