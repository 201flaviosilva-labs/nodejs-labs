// Arrays Methods

console.log("Array");
let arrayStrings = ["Java", "JavaScript", "HTML","CSS","React"];
console.log(arrayStrings);

// Número de elementos (length)
arrayStrings.length;

// Ordena o Array por ordem alfabetica (sort)
arrayStrings.sort(); 

// Converter um array em uma Strings (toString)
arrayStrings.toString();

// Converter um array em String com um caracter entre cada um dos elementos (join)
arrayStrings.join(" | ");


// --- Metodos mais especificos (CRUD)

// Adicionar
// Adicionar um novo elemento no final (push)
arrayStrings.push("Limão");

// Adicionar um elemento no inicio do erray (unshift)
arrayStrings.unshift("Maça");


// Remover
// Remover o ultimo elemento de um arrray (pop)
arrayStrings.pop();

// Remover o primeiro elemento do array (shift)
arrayStrings.shift();

// Remover varios elementos (splice)
arrayStrings.splice(3,1); //Remove "JavaScript"


// Alterar
// Aleter um elemento na posição expecifica (splice)
delete arrayStrings[1]; // modifica o elemento 2 ( começando a contar no 1) para undefined

// Remover e adicionar elementos
arrayStrings.splice(1, 1, "Açucar", "Pera");
// O primeiro parametro refere onde os elementos vão começar a ser adicionados
// O segundo parametro refere quantos elementos vão ser eliminados
// A partir do terceiro parametro é os elementos que vaão ser adicionados


// --------  Metodos que criam novos Arrays
// Juntar dois Arrays (concat)
const newArray1 = ["Fogo", "Água","Terra","Pedra"];
const arrayJunto = arrayStrings.concat(newArray1);
// Para juntar mais que dois Arrays
//const arrayJunto = arrayStrings.concat(newArray1, newArray2, ["Bananas", "Arroz"]);


//Criar um array com alguns elementos do array antigo
const algunsElementos1 = arrayStrings.slice(1);
// O argumento indica de onde quer começar a copiar o array
const algunsElementos2 = arrayStrings.slice(0,2);
// O segundo argomento indica quantos quer copiar

console.log(arrayStrings);