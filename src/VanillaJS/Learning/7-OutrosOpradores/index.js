/*
Type

typeof -	Returns the type of a variable
instanceof -	Returns true if an object is an instance of an object type

Bitwise

&	AND	5 & 1	0101 & 0001	0001	 1
|	OR	5 | 1	0101 | 0001	0101	 5
~	NOT	~ 5	 ~0101	1010	 10
^	XOR	5 ^ 1	0101 ^ 0001	0100	 4
<<	Zero fill left shift	5 << 1	0101 << 1	1010	 10
>>	Signed right shift	5 >> 1	0101 >> 1	0010	  2
>>>	Zero fill right shift	5 >>> 1	0101 >>> 1	0010	  2
*/
const y = 2;
console.log(typeof "Zé");
console.log(typeof 3.14);
console.log(typeof true);
console.log(typeof x);
console.log(typeof y);