console.log("Opradores Logicos");

/*
&&	logical and
||	logical or
!	logical not
*/

console.log(true && true); //true
console.log(true && false); //false

console.log(true || true); //true
console.log(true || false); //true

console.log(!(true && true)); //false
console.log(!(true && false)); //true

console.log(!(true || true)); //false
console.log(!(true || false)); //false