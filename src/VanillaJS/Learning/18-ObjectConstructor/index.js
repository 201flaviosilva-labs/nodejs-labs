// The Object Constructor

//Simple Object Creation
const person = {
  name:"Silva",
  age:"10"
}
console.log("Pessoa: "+person);
console.log("Nome pessoa: " + person.name);

// An Object Constructor Function
function persons(name, age){
  this.name=name; //this -> Refers at the arguments of the function
  this.age=age;

}

//Now just creat new persons with the same attributes
const p1 = new persons("Silva", 10);
const p2 = new persons("Ricardo", 11);

console.log("Pessoa1: " + p1.name);
console.log("Pessoa2: " + p2.age);