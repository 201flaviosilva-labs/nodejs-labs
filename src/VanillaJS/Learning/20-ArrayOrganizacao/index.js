// Metodos de Organização de Arrays
let frutas = ["Manga", "Banana", "Laranja", "Maça", "Pera","coco"];

// Organiza o array por ordem alfabética
frutas.sort();

// Mete o array ao contrário
frutas.reverse();

let numeros = [40, 100, 1, 5, 25, 10];
numeros.sort(function(a, b){return a - b});
// numeros.sort(function(a, b){return b - a})

// Descubrir o maior e o menor
Math.max.apply(null, numeros); // Maior dentro do array
Math.min.apply(null, numeros); // Menor do array

// Organizar um arrau por objeto
const caros = [
  {marca:"Volvo", ano:2016},
  {marca:"Saab", ano:2001},
  {marca:"BMW", ano:2010}
];
caros.sort(function(a, b){return a.ano - b.ano});
