const express = require("express");
const path = require("path");

const app = express();

app.use(express.static(path.join(__dirname, "public")));

const PORT = 3001 || process.env.PORT;
app.listen(PORT, () => {
	console.log("Porta: " + PORT);
	console.log(`http://localhost:${PORT}/`);
});
