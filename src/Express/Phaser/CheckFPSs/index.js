// imports for  phaser
require("@geckos.io/phaser-on-nodejs");
const Phaser = require("phaser");

class MainScene extends Phaser.Scene {
	constructor() {
		super("MainScene");
	}

	init() {
		console.log("Init");
	}

	create() {
		console.log("Create");
	}

	update() {
		console.log("Update: " + game.loop.actualFps.toFixed(1));
	}
}

const config = {
	type: Phaser.HEADLESS,
	banner: false,
	audio: false,
	fps: {
		target: 1000,
		forceSetTimeOut: true,
	},
	scene: [MainScene],
}

const game = new Phaser.Game(config);
