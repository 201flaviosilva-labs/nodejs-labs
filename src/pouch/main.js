var PouchDB = require('pouchdb');
var db = new PouchDB('db'); // Criar Base de Dados

main();
function main() {
	criarDoc("Doc1");
	editarDoc("Doc1Editado");
	procurarDoc("1");
	apagarDoc("1");
	apagarDB();
}

function criarDoc(nome) {
	db.put({
		_id: "1",
		title: nome
	}, function (err, response) {
		if (err) return console.log(err);
		else console.log("Doc Criado");
	});
}

function editarDoc(nome) {
	db.post({
		title: nome
	}, function (err, response) {
		if (err) return console.log(err);
		else console.log("Doc Editado");
	});
}

function procurarDoc(id) {
	db.get(id, function (err, doc) {
		if (err) return console.log(err);
		else console.log(doc);
	});
}

function apagarDoc(id) {
	db.get(id, function (err, doc) {
		if (err) { return console.log(err); }
		db.remove(doc, function (err, response) {
			if (err) return console.log(err);
			else console.log("Doc Apagado");
		});
	});
}

function apagarDB() {
	// Delete Base de Dados
	db.destroy(function (err, response) {
		if (err) console.log(err);
		else console.log("success");
	});
}
